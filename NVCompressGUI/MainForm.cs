﻿/*
 * Created by SharpDevelop.
 * User: nuke
 * Date: 14.04.2013
 * Time: 16:57
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Threading;


namespace NVCompressGUI
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		
		string sourcePath = "no";
		string targetPath = "no";
		string preparedTargetPath = "empty";
		int fileCount = 0;
		int i = 0;
		double prozent = 0.00000f;
		string arguments = "-nomips ";
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			textBox1.Text = "No Folder set yet";
			textBox2.Text = "No Folder set yet";
			comboBox1.SelectedItem = "DXT1";
			radioButton1.Checked = true;
			textBox6.Text = "0 %";
			System.Drawing.Bitmap bmp = DevIL.DevIL.LoadBitmap(@"C:\Users\Dell435MT\Desktop\dds\0.0242.dds");
			pictureBox1.Image = bmp;
			
			
			int nvcompressfile = Directory.GetFiles("bin\\", "nvcompress.exe").Length;
			int cuda32file = Directory.GetFiles("bin\\", "cudart32_30_14.dll").Length;
			
			if (nvcompressfile == 0) 
			{
				MessageBox.Show("nvcompress.exe not found");
				Close();
			}
			
			if (cuda32file == 0) 
			{
				MessageBox.Show("cudart32_30_14.dll not found, probably not 32bit Version");
				Close();
			}
			                                   
			
			
		}
		
		
		
		void Button1Click(object sender, EventArgs e)
		{
			FolderBrowserDialog objDialog = new FolderBrowserDialog();
			DialogResult objResult = objDialog.ShowDialog(this);
			if (objResult == DialogResult.OK) 
				{
				textBox1.Text = objDialog.SelectedPath;
				
				sourcePath = objDialog.SelectedPath;
				fileCount = Directory.GetFiles(sourcePath, "*.png").Length;
				if (fileCount == 0) MessageBox.Show("No PNG's found");
				else
					{
					prozent = 100d / fileCount;
								
					}
				textBox5.Text = Convert.ToString(fileCount);	
				}
    			else
    			MessageBox.Show("Folder?");
			
		}
		
		
		
		void TextBox1TextChanged(object sender, EventArgs e)
		{
			
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			
			FolderBrowserDialog objDialog = new FolderBrowserDialog();
			DialogResult objResult = objDialog.ShowDialog(this);
			if (objResult == DialogResult.OK) 
				{
				textBox2.Text = objDialog.SelectedPath;
				targetPath = objDialog.SelectedPath;
				}
    			else
    			MessageBox.Show("Folder?");	
		}
		
		void Label1Click(object sender, EventArgs e)
		{
			
		}
		
		void RadioButton1CheckedChanged(object sender, EventArgs e)
		{
			
		}
		
		void Label2Click(object sender, EventArgs e)
		{
			
		}
		
		void Button3Click(object sender, EventArgs e)
		{
			
  			
  			
  			if ( sourcePath != "no" && targetPath != "no") 
  			{
  				arguments = "";
  				
  				//mipmaps or not ?
  				if ( checkedListBox1.GetItemCheckState(0).ToString() == "Unchecked") arguments = "-nomips "; else arguments = "";
  				
  				// hier texture mapping auswählen
  				if (radioButton2.Checked == true) arguments = "-repeat " + arguments; else arguments = "-clamp " + arguments;
  				
  				//alpha or not ?
  				if ( checkedListBox1.GetItemCheckState(1).ToString() == "Checked") arguments = arguments + "-alpha "; 
  				
  				//normal or not ?
  				if ( checkedListBox1.GetItemCheckState(2).ToString() == "Checked") arguments = arguments + "-normal "; 
  				
  				//tonormal or not ?
  				if ( checkedListBox1.GetItemCheckState(3).ToString() == "Checked") arguments = arguments + "-tonormal ";
  				
  				
  				//texturformate
  				switch(comboBox1.SelectedIndex)
       			 {
        				case 0:
  						arguments = "-bc1 " + arguments;
            				break;
        				case 1:
            				arguments = "-rgb " + arguments;
            				break;
        				case 2:
            				arguments = "-bc1n " + arguments;
            				break;
            			case 3:
            				arguments = "-bc1a " + arguments;
            				break;
            			case 4:
            				arguments = "-bc2 " + arguments;
            				break;
            			case 5:
            				arguments = "-bc3 " + arguments;
            				break;
            			case 6:
            				arguments = "-bc3n " + arguments;
            				break;
            			case 7:
            				arguments = "-bc4 " + arguments;
            				break;
            			case 8:
            				arguments = "-bc5 " + arguments;
            				break;
        				}
  				
  				textBox7.Text = arguments;
  				
  				
  				foreach (string file in Directory.GetFiles(sourcePath, "*.png"))
				{
  					string bufferPath = "";
  					// remove spaces from filenames
  					if (checkedListBox1.GetItemCheckState(5).ToString() == "Checked") 
  					{
  						bufferPath = file.Replace(sourcePath,"");
	  					bufferPath = bufferPath.Replace(" ","_");
	  					bufferPath = sourcePath + bufferPath;
	  					try 
	  					{
	  						System.IO.File.Move(@file, @bufferPath);	 //	rename file if spaces
	  					}
  						catch
  						{
  							MessageBox.Show("Could not fix Filename, file in use !");
  							Close();
  						}
  					}
  					else bufferPath = file;
  					
  					// loadImagePreview
  					if (checkBox2.Checked == true)
  					{
  						Image image = Image.FromFile(bufferPath);
  						pictureBox1.Image = image;
  						textBox3.Text = (Convert.ToString(image.Height) + "x" + Convert.ToString(image.Width));
  					}
  					
  					// convert images to power of two
  					if (checkedListBox1.GetItemCheckState(4).ToString() == "Checked") 
  					{
  						Image image = Image.FromFile(bufferPath);
  						pictureBox1.Image = image;
  						
  						textBox8.Text = (Convert.ToString(PowerOfTwo(image.Height)) + "x" + Convert.ToString(PowerOfTwo(image.Width)));
  					
  						//convert ausführen
  						Process MyConvert = new Process();
	  					MyConvert.StartInfo.FileName = @"bin\convert.exe";
	 					MyConvert.StartInfo.Arguments = @bufferPath + " -filter Lanczos -sampling-factor 1x1 -quality 90 -resize " + Convert.ToString(PowerOfTwo(image.Width)) + "x" + Convert.ToString(PowerOfTwo(image.Height)) + "! " + @targetPath + @"\bufferimage.png";
	  					MyConvert.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
	  					MyConvert.StartInfo.UseShellExecute = false;
  						MyConvert.StartInfo.CreateNoWindow = true;
  						MyConvert.Start();
	  					MyConvert.WaitForExit();
  					}
  					
  					
  					
  					
  					
  					
  			
	  				// nvcompress ausführen
	  				preparedTargetPath = bufferPath.Replace(sourcePath,targetPath);
	  				preparedTargetPath = preparedTargetPath.Replace("png","dds");	
	  				Process SomeProgram = new Process();
	  				SomeProgram.StartInfo.FileName = @"bin\nvcompress.exe";
	 				// if power of 2 is on then use bufferimage as source
	  				if (checkedListBox1.GetItemCheckState(4).ToString() == "Checked") SomeProgram.StartInfo.Arguments = @arguments + @targetPath + @"\bufferimage.png" + " " + @preparedTargetPath;
	  				else SomeProgram.StartInfo.Arguments = @arguments + @bufferPath + " " + @preparedTargetPath;
	  				SomeProgram.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
	  				SomeProgram.StartInfo.UseShellExecute = false;
	  				//Commandline Windows an oder aus
	  				if (checkBox1.Checked == false) SomeProgram.StartInfo.CreateNoWindow = true;
  			
  				
	  				SomeProgram.Start();
	  				SomeProgram.WaitForExit();
	  				i++;
	  				textBox4.Text = file;
	  				textBox6.Text = Convert.ToString(Convert.ToInt32(i*prozent)) + " %";
	  				this.Refresh();
  				
  				}
  				if (checkedListBox1.GetItemCheckState(4).ToString() == "Checked") System.IO.File.Delete(@targetPath + @"\bufferimage.png");
  				textBox6.Text = "100 %";
  				MessageBox.Show(Convert.ToString(i) + " Files processed");
  				textBox6.Text = "0 %";
  				textBox4.Text = "";
  				//textBox3.Text = "";
  				i = 0;
  			}
  			else
  			MessageBox.Show("Please choose source and target folder !");	
  		}
		
		double PowerOfTwo(int Res)
		{
			double Ergebnis = 0;
			Ergebnis = Math.Pow(2,Math.Ceiling(Math.Log(Res,2)));
			return Ergebnis;
		}
		
		void ProgressBar1Click(object sender, EventArgs e)
		{					}
		
		void Label4Click(object sender, EventArgs e)
		{
			
		}
		
		void CheckBox4CheckedChanged(object sender, EventArgs e)
		{
			
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{
			
		}
		
		void CheckedListBox1SelectedIndexChanged(object sender, EventArgs e)
		{
		if (checkedListBox1.GetItemCheckState(4).ToString() == "Checked") 
		{checkBox2.Visible = false;
			this.Refresh();
		}
			
		else 
		{checkBox2.Visible = false;
		this.Refresh();
		}
  						
		}
	}
}
