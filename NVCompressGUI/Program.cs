﻿/*
 * Created by SharpDevelop.
 * User: nuke
 * Date: 14.04.2013
 * Time: 16:57
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;

namespace NVCompressGUI
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			//dll's in subfolder
			AppDomain.CurrentDomain.AppendPrivatePath("bin");
			Application.Run(new MainForm());
			
		}
		
	}
}
